import xarray as xr
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from datetime import datetime
import pickle
import time

def Save_Var(data, save_name):
    with open(str(save_name + ".data"), 'wb') as f:
        pickle.dump(data, f)
        
    try:
        with open(str(save_name + ".data"), 'rb') as f:
            pickle.load(f)
    except:
        print("Error: saving not successful")
    else:
        print(str(save_name + ".data" + " saved succesfully"))



def Load_Var(save_name):
    with open(save_name, 'rb') as f:
        data = pickle.load(f)
    return data 

def Examine_Nulls(df):
    percent_missing = df.isnull().sum() * 100 / len(df)
    missing_value_df = pd.DataFrame({'column_name': df.columns,
                                 'percent_missing': percent_missing})
    missing_value_df.sort_values('percent_missing', inplace=True)
    return missing_value_df

def replace_nulls(x):
    if pd.isnull(x['preds_iso']) is True:
        if x['tmin'] > 0 and x['tmax'] > 0:
            return 0
        elif x['tmin'] < 0 and x['tmax'] < 0:
            return 1
        else: 
            if x['tmid'] < 0:
                return 1
            elif x['tmid'] > 0:
                return 0
    else:
        return x['preds_iso']

# Load Data
start = time.time()
print("Loading data...")
DS = xr.open_dataset('SCDNA_v1.1.nc4')
print("Data loaded")
print()

IDs = DS.ID.values.astype(str)
stationid = [c2+c3+c4+c5+c6+c7+c8+c9+c10+c11+c12 for (c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12)           in zip(IDs[2,:],IDs[3,:],IDs[4,:],IDs[5,:],IDs[6,:],IDs[7,:],IDs[8,:],IDs[9,:],IDs[10,:],                  IDs[11,:],IDs[12,:])]
sources = [c0+c1 for (c0,c1) in zip(IDs[0,:],IDs[1,:])]
oid = [source+sid for (source,sid) in zip(sources,stationid)]

# Modify XArray object for easier conversion to a Dataframe
print("Processing XArray dataset..")
dates = [datetime.strptime(str(date),'%Y%m%d') for date in DS.date.values]
DS = DS.set_coords(['date','ID'])
DS = DS.assign_coords(nstn = oid, nday = dates)

DS = DS.drop_dims(['lle'])
DS = DS.drop_vars("date")
DS = DS.rename_dims(nstn='station', nday='datetime')
DS = DS.rename_vars(nstn='station', nday='datetime')
print("XArray dataset processed")
print()

# Only select stations w/ temperature and precip data
print("Finding valid stations...")
valid_stns = ~np.isnan(DS.prcp.values).any(axis=1) & ~np.isnan(DS.tmin.values).any(axis=1) & ~np.isnan(DS.tmax.values).any(axis=1)
print("Valid stations found")
print()

# Batches
n_samples = 0
precip_thresh = 1
limit = np.sum(valid_stns)
bsize = 100

# Load Model
print("Loading model...")
ir_era5 = Load_Var('iso_t2m.data')
print("Model loaded")
print()

while n_samples < limit:
    prev = n_samples
    if n_samples < 16900:      
        n_samples += bsize
    else:
        n_samples += 37
    print(prev, ':', n_samples)
    print('--------------------------------------------')
    print("Processing Samples: {}/{}".format(n_samples, limit))
    print('--------------------------------------------')
    
    # Convert XArray to Dataframe
    print("Converting XArray dataset to dataframe...")
    precip_ = DS.prcp.loc[valid_stns][prev:n_samples]
    precip_df = precip_.to_dataframe().reset_index()
    precip_df = precip_df[precip_df['prcp'] >= precip_thresh]
    
    tmin_ = DS.tmin.loc[valid_stns][prev:n_samples]
    tmin_df = tmin_.to_dataframe().reset_index()

    tmax_ = DS.tmax.loc[valid_stns][prev:n_samples]
    tmax_df = tmax_.to_dataframe().reset_index()

    full_df = pd.merge(
        precip_df, 
        pd.merge(tmax_df, tmin_df, on=['station', 'datetime']),
        on=['station', 'datetime']
    )
    full_df.set_index(['station', 'datetime'], inplace=True)
    print("Dataset converted to dataframe")
    print()

    print("Calculating predictions...")
    preds_iso = []
    for i, j in zip(full_df['tmin'], full_df['tmax']):
        if i == j:
            preds_iso.append(
                np.mean(ir_era5.predict([i]))
            )
        else:
            vals = np.linspace(i, j, 100)
            preds = ir_era5.predict(vals)
            preds_iso.append(np.mean(preds))        


    full_df['preds_iso'] = np.float32(list(preds_iso))
    full_df['tmid'] = full_df.apply(
        lambda row: np.float32(np.mean([row['tmin'], row['tmax']])), axis=1
    )
    full_df['tmid'] = full_df['tmid'].astype(np.float32)
    full_df['filled_preds_iso'] = full_df.apply(replace_nulls, axis=1)
    full_df['filled_preds_iso'] = full_df['filled_preds_iso'].astype(np.float32)
    full_df.drop('preds_iso', axis=1, inplace=True)
    print("Predictions finished")
    print()

    print("Plotting predictions...")
    plt.scatter(full_df['tmid'], preds_iso, s=0.7)
    plt.title("SCDNA Isotonic Regression")
    plt.show()
    plt.close('all')
    print("Predictions plotted")
    print()

    print("Saving to .CSV file...")
    full_df.to_csv('C:\\Users\\quintoj\\Desktop\\SCDNA\\CSV\\test_{}.csv'.format(n_samples))
    

    print(".CSV file saved")
    print()
end = time.time()
print("Time Elapsed:", end-start)
