# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
import calendar
from scipy.optimize import curve_fit 
from sklearn.isotonic import IsotonicRegression

def Save_Var(data, save_name):
    with open(str(save_name + ".data"), 'wb') as f:
        pickle.dump(data, f)
        
    try:
        with open(str(save_name + ".data"), 'rb') as f:
            new_data = pickle.load(f)
    except:
        print("Error: saving not successful")
    else:
        print(str(save_name + ".data" + " saved succesfully"))

def Load_Var(save_name):
    with open(save_name, 'rb') as f:
        data = pickle.load(f)
    return data 

def logistic4(x, k, mu):
    a = 1 
    c = 0
    return a + (c-a)/(1+np.exp(-k*(x-mu))) 

def myfit(func, x, y):
    xr = x.ravel()
    yr = y.ravel()
    asort = np.argsort(xr)
    popt, pcov = curve_fit(func, xr[asort], yr[asort])
    res = yr[asort]-func(xr[asort], *popt)
    return popt, res # gives error b/w every pred and true point -> useful for validation -> MSE

def RMSE(calculate_error, yData, resid=None, yPred=None, params=None):
    if calculate_error is False and resid is not None:
        SE = np.square(resid) # squared errors
        MSE = np.mean(SE) # mean squared errors
        RMSE = np.sqrt(MSE) # Root Mean Squared Error, RMSE
        Rsquared = 1.0 - (np.var(resid) / np.var(yData))
    elif calculate_error is True and yPred is not None:
        resid = yData - yPred
        SE = np.square(resid) # squared errors
        MSE = np.mean(SE) # mean squared errors
        RMSE = np.sqrt(MSE) # Root Mean Squared Error, RMSE
        Rsquared = 1.0 - (np.var(resid) / np.var(yData))
    if params is not None:
        print('Parameters:', params)
    print('RMSE:', RMSE)
    print('R-squared:', Rsquared)
    return [RMSE, Rsquared]

def Heaviside(x, a ,b, c): 
    return a * (np.sign(x - b) + c) # Heaviside function

precip_df = pd.read_csv('AHCCD_precip_processed.csv', index_col=['ClimID', 'Date'])
precip_df.reset_index(inplace=True)
precip_df.rename(columns={'ClimID' : 'Station'}, inplace=True)
precip_df['Date'] = pd.to_datetime(precip_df['Date'])
precip_df['Station'] = precip_df['Station'].astype(str) ## VERY IMPORTANT

temp_df = pd.read_csv('AHCCD_full.csv')
temp_df['Date'] = pd.to_datetime(temp_df['Date'])

merged_df_ = pd.merge(precip_df, temp_df, on=['Station', 'Date'])

months = np.unique(merged_df_['Date'].dt.month)
params_era5 = [1.36527176, 0.98420547]
t2m_thresh = 1.0116516113281477
ir_era5 = Load_Var('iso_t2m.data')

isos = []
logs = []
heavs = []

print('--------------------------------------')
for m in months:
    print(calendar.month_name[m])
    print('--------------------------------------')
    merged_df = merged_df_[merged_df_['Date'].dt.month == m]
    merged_df.set_index(['Station', 'Date'], inplace=True)
    merged_df = merged_df[['Mean_Temp', 'snow_frac']]
    
    x = merged_df['Mean_Temp']
    y = merged_df['snow_frac']
    x_lr = np.linspace(merged_df['Mean_Temp'].min(), merged_df['Mean_Temp'].max(), 1000)
    y_log_era5 = logistic4(x, *params_era5)
    
    print('Generalized Logistic Regression')
    logs.append(RMSE(params=params_era5, calculate_error=True, yData=y, yPred=y_log_era5))
    print()
    
    y_ir_era5 = ir_era5.predict(x)
    yy_ir_era5 = ir_era5.predict(x_lr)
    
    print("Isotonic Regression")
    isos.append(RMSE(calculate_error=True, yData=y, yPred=y_ir_era5))
    print()
    
    y_heaviside_ = Heaviside(x, -0.5, t2m_thresh, -1)
    print("Heaviside Function")
    heavs.append(RMSE(calculate_error=True, yData=y, yPred=y_heaviside_))
    print('--------------------------------------')
    
    plt.rcParams["axes.labelsize"] = 12
    g = sns.jointplot(
        x=merged_df['Mean_Temp'], 
        y=merged_df['snow_frac']*100, 
        kind='hex', 
        bins='log', 
        height=7
    )
    sns.lineplot(
        x_lr, 
        Heaviside(x_lr, -50, t2m_thresh, -1), 
        color='g', 
        linewidth=2.5, 
        label='Heaviside', 
        ax=g.ax_joint
    )
    sns.lineplot(
        x=x_lr, 
        y=(logistic4(x_lr, *params_era5))*100, 
        color='#ffcc00', 
        label= 'Generalized Logistic', 
        ax=g.ax_joint, 
        linewidth=2.5
    )
    sns.lineplot(
        x=x_lr, 
        y=[i*100 for i in yy_ir_era5], 
        color='red', 
        label= 'Isotonic Regression', 
        ax=g.ax_joint, 
        linewidth=2.5
    )
    g.ax_joint.legend(prop={"size": 8}, loc='lower left')
    g.ax_joint.set(xlabel='Daily Mean Temperature ($^\circ$C)', ylabel='Snow Fraction (%)', xlim=(-40, 30))
    cbar_ax = g.fig.add_axes([1, .1, .05, .75])  # x, y, width, height
    plt.colorbar(cax=cbar_ax)
    plt.savefig(str("Monthly_no_interp//" + calendar.month_name[m] + ".png"))
    plt.show()

Save_Var(isos, 'isos')
Save_Var(logs, 'logs')
Save_Var(heavs, 'heavs')

fig, ax = plt.subplots(1,1, figsize=(15, 10))
ax.plot([calendar.month_name[i] for i in months], [i[0] for i in isos], label="Isotonic Regression")
ax.plot([calendar.month_name[i] for i in months], [i[0] for i in logs], label="Generalized Logistic Regression")
ax.plot([calendar.month_name[i] for i in months], [i[0] for i in heavs], label="Heaviside Function")
ax.set_ylabel('RMSE')
ax.set_xlabel('Month')
plt.xticks(rotation=60)
plt.legend(loc='lower left')
plt.tight_layout()
plt.savefig('monthly_errors.png')
plt.show()

fig, ax = plt.subplots(1,1, figsize=(15, 10))
ax.plot([calendar.month_name[i] for i in months], [i[1] for i in isos], label="Isotonic Regression")
ax.plot([calendar.month_name[i] for i in months], [i[1] for i in logs], label="Generalized Logistic Regression")
ax.plot([calendar.month_name[i] for i in months], [i[1] for i in heavs], label="Heaviside Function")
ax.set_ylabel('R$^2$')
ax.set_xlabel('Month')
plt.xticks(rotation=60)
plt.legend(loc='lower left')
plt.tight_layout()
plt.savefig('monthly_rsquared.png')
plt.show()



